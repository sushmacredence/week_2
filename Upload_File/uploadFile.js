const express = require('express');
const multer = require('multer');
const app = express();

var storage =   multer.diskStorage({
    destination:'./uploads',
    filename: function (req, file, callback) {
      callback(null, file.originalname);
    }
  });
  var  upload = multer({ storage : storage});

app.get("/", function(req,res){
    res.sendFile(__dirname + '/index.html');
});

app.post("/upload",upload.single('upfile'), function(req,res){
    //res.redirect('/');
    res.send("File uploaded successfully");
});

app.listen(3001, function (){
    console.log("Listening to port 3001...")
});