var express = require('express');
var app = express();
const oracledb = require('oracledb');
var db = require('./db');
var bodyparser = require('body-parser');

app.use(bodyparser.json());
oracledb.autoCommit = true;

//Select All
app.get('/selectall',async function(req, res){
    try{
        var connection = await oracledb.getConnection(db);
        var result = await connection.execute(`select * from emp`);
        console.log(result.rows);
        res.send(result.rows);
    }
    catch(err){
        console.log("ERROR: ", err);
    }
    finally{
        connection.close();
    }
});
//Select by ID 
app.get('/select/:id',async function(req, res){
    try{
        var connection = await oracledb.getConnection(db);
        var result = await connection.execute(`select * from emp where id= :id`,{id : req.params.id});
        console.log(result.rows);
        res.send(result.rows);
    }
    catch(err){
        console.log("ERROR: ", err);
    }
    finally{
        connection.close();
    }
});
//Insert a data
app.post("/send", async function(req,res){
    try{
        var connection = await oracledb.getConnection(db);
        var data = {
            id : req.body.id,
            name : req.body.name,
            dept : req.body.dept
        }
        var result = await connection.execute(`insert into emp(id,name,dept) values(:id,:name,:dept)`,data);
        console.log("Inserted Sucessfully");
        res.send("Inserted Sucessfully");
    }
    catch(err){
        console.log("ERROR: ", err);
    }
    finally{
        connection.close();
    }
});
//Update a data
app.put("/update/:id", async function(req,res){
    try{
        var connection = await oracledb.getConnection(db);
        var data = {
            id : req.params.id,
            name : req.body.name,
            dept : req.body.dept
        }
        var result = await connection.execute(`update emp set name=:name, dept= :dept where id=:id`,data);
        console.log("Updated Sucessfully");
        res.send("Updated Sucessfully");
    }
    catch(err){
        console.log("ERROR: ", err);
    }
    finally{
        connection.close();
    }
});
//Delete a data
app.delete("/delete/:id", async function(req,res){
    try{
        var connection = await oracledb.getConnection(db);
        var data = {id : req.params.id};
        var result = await connection.execute(`delete from emp where id=:id`,data);
        console.log("Deleted Sucessfully");
        res.send("Deleted Sucessfully");
    }
    catch(err){
        console.log("ERROR: ", err);
    }
    finally{
        connection.close();
    }
});

app.listen(5000,  function(req,res){
    console.log("Listening to port 5000...");
}); 